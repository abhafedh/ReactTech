import React from 'react';

import { Transactions } from '../Transactions';
import { Sidebar } from '../../Components/Sidebar';

require('./PageLayout.css');

export const PageLayout = () => {
  return (
    <div className="container">
      <Sidebar />
      <Transactions />
    </div>
  );
};
