import React from 'react';
import Moment from 'react-moment';
// import axios from 'axios';

const data = require('../../data/transactions.json');
require('./Transactions.css');

export class Transactions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transactions: [],
    };
  }

  componentDidMount() {
    console.log(data);
    this.setState({ transactions: data[0].transactions }, () => {
      console.log('data : ', this.state.transactions);
    });
    // axios.get('./transactions.json').then(res => {
    //   const transactions = res;
    //   this.setState({ transactions }, () => {
    //     console.log(this.state);
    //   });
    // });
  }

  render() {
    const { transactions } = this.state;
    return (
      <div>
        <span className="bar" />
        <div className="list">
          <ul className="head">
            <li>DD-MM-YYYY</li>
            <li>Name</li>
            <li>Payement</li>
            <li>Amount</li>
          </ul>
          <ul className="list">
            {transactions.map((value, index) => (
              <li key={index}>
                <span>
                  <Moment format="YYYY-MM-DD">{value.created_at}</Moment>
                </span>
                <span>{value.counterparty_name}</span>
                <span>{value.operation_type}</span>
                <span>
                  {value.amount}
                  <i />
                </span>
                <span>icon {value.attachements.length}</span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}
