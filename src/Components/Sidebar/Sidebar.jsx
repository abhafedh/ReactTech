import React from 'react';
require('./Sidebar.css');

export const Sidebar = () => {
  return (
    <div className="sidebar">
      <span className="title">FINPAL</span>
      <ul>
        <li>
          <a href="/">Overview</a>
        </li>
        <li>
          <a href="/">Transactions (3)</a>
        </li>
      </ul>
      <ul>
        <li>
          <a href="/">Transfers (2)</a>
        </li>
        <li>
          <a href="/">Invoices (1)</a>
        </li>
      </ul>

      <ul>
        <li>
          <a href="/">Team</a>
        </li>
        <li>
          <a href="/">Intgrations</a>
        </li>
        <li>
          <a href="/">Settings</a>
        </li>
      </ul>
      <button>upgrade account</button>
    </div>
  );
};
