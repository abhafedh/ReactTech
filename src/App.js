import React, { Component } from 'react';
import { PageLayout } from './Containers/PageLayout';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <PageLayout />
      </div>
    );
  }
}

export default App;
